/*
 * uart0.c
 *
 *  Created on: May 8, 2013
 *      Author: renan
 */


#include "uart0.h"

void UART0_Init (int baudrate)
{
	int pclk;
	unsigned long int Fdiv;

	// PCLK_UART0 is being set to 1/4 of SystemCoreClock
	pclk = SystemCoreClock / 4;

	// Turn on power to UART0
	LPC_SC->PCONP |=  PCUART0_POWERON;

	// Turn on UART0 peripheral clock
	LPC_SC->PCLKSEL0 &= ~(PCLK_UART0_MASK);
	LPC_SC->PCLKSEL0 |=  (0 << PCLK_UART0);		// PCLK_periph = CCLK/4

	// Set PINSEL0 so that P0.2 = TXD0, P0.3 = RXD0
	LPC_PINCON->PINSEL0 &= ~0xf0;
	LPC_PINCON->PINSEL0 |= ((1 << 4) | (1 << 6));

	LPC_UART0->LCR = 0x83;				// 8 bits, no Parity, 1 Stop bit, DLAB=1
	Fdiv = ( pclk / 16 ) / baudrate ;	// Set baud rate
	LPC_UART0->DLM = Fdiv / 256;
	LPC_UART0->DLL = Fdiv % 256;
	LPC_UART0->LCR = 0x03;				// 8 bits, no Parity, 1 Stop bit DLAB = 0

	// Configure FIFO
	uint8_t tmp = 0;
	tmp |= UART_FCR_FIFO_EN;
	tmp |= UART_FCR_TRG_LEV0;
	tmp |= UART_FCR_TX_RS;
	tmp |= UART_FCR_RX_RS;

	LPC_UART0->FCR = tmp & UART_FCR_BITMASK;

	// Enable transmission on UART TxD pin
	// Enable UART Transmit
	LPC_UART0->TER |= UART_TER_TXEN;
}

/*********************************************************************//**
 * @brief		Transmit a single data through UART peripheral
 * @param[in]	Data	Data to transmit (must be 8-bit long)
 * @return 		None
 **********************************************************************/
void UART_SendByte (uint8_t Data)
{
	LPC_UART0->THR = Data & UART_THR_MASKBIT;
}


/*********************************************************************//**
 * @brief		Receive a single data from UART peripheral
 * @return 		Data received
 **********************************************************************/
uint8_t UART_ReceiveByte ()
{
	return (LPC_UART0->RBR & UART_RBR_MASKBIT);
}

/*********************************************************************//**
 * @brief		Receive a block of data via UART peripheral
 * @param[out]	rxbuf 	Pointer to Received buffer
 * @param[in]	buflen 	Length of Received buffer
 * @param[in] 	flag 	Flag mode, should be NONE_BLOCKING or BLOCKING

 * @return 		Number of bytes received
 *
 * Note: when using UART in BLOCKING mode, a time-out condition is used
 * via defined symbol UART_BLOCKING_TIMEOUT.
 **********************************************************************/
uint32_t UART_Receive (uint8_t *rxbuf, uint32_t buflen, TRANSFER_BLOCK_Type flag)
{
	uint32_t bToRecv, bRecv, timeOut;
	uint8_t *pChar = rxbuf;

	bToRecv = buflen;

	// Blocking mode
	if (flag == BLOCKING)
	{
		bRecv = 0;
		while (bToRecv)
		{
			timeOut = UART_BLOCKING_TIMEOUT;
			while (!(LPC_UART0->LSR & UART_LSR_RDR))
			{
				if (timeOut == 0)
					break;
				timeOut--;
			}
			// Time out!
			if(timeOut == 0)
				break;
			// Get data from the buffer
			(*pChar++) = UART_ReceiveByte ();
			bToRecv--;
			bRecv++;
		}
	}
	// None blocking mode
	else
	{
		bRecv = 0;
		while (bToRecv)
		{
			if (!(LPC_UART0->LSR & UART_LSR_RDR))
				break;
			else
			{
				(*pChar++) = UART_ReceiveByte();
				bRecv++;
				bToRecv--;
			}
		}
	}
	return bRecv;
}

/*********************************************************************//**
 * @brief		Send a block of data via UART peripheral
 * @param[in]	txbuf 	Pointer to Transmit buffer
 * @param[in]	buflen 	Length of Transmit buffer
 * @param[in] 	flag 	Flag used in  UART transfer, should be
 * 						NONE_BLOCKING or BLOCKING
 * @return 		Number of bytes sent.
 *
 * Note: when using UART in BLOCKING mode, a time-out condition is used
 * via defined symbol UART_BLOCKING_TIMEOUT.
 **********************************************************************/
uint32_t UART_Send(uint8_t *txbuf, uint32_t buflen, TRANSFER_BLOCK_Type flag)
{
	uint32_t bToSend, bSent, timeOut, fifo_cnt;
	uint8_t *pChar = txbuf;

	bToSend = buflen;

	// blocking mode
	if (flag == BLOCKING)
	{
		bSent = 0;
		while (bToSend)
		{
			timeOut = UART_BLOCKING_TIMEOUT;
			// Wait for THR empty with timeout
			while (!(LPC_UART0->LSR & UART_LSR_THRE))
			{
				if (timeOut == 0)
					break;
				timeOut--;
			}
			// Time out!
			if(timeOut == 0)
				break;
			fifo_cnt = UART_TX_FIFO_SIZE;
			while (fifo_cnt && bToSend)
			{
				UART_SendByte (*pChar++);
				fifo_cnt--;
				bToSend--;
				bSent++;
			}
		}
	}
	// None blocking mode
	else
	{
		bSent = 0;
		while (bToSend)
		{
			if (!(LPC_UART0->LSR & UART_LSR_THRE))
				break;
			fifo_cnt = UART_TX_FIFO_SIZE;
			while (fifo_cnt && bToSend)
			{
				UART_SendByte (*pChar++);
				bToSend--;
				fifo_cnt--;
				bSent++;
			}
		}
	}
	return bSent;
}

/*********************************************************************//**
 * @brief		Check whether if UART is busy or not
 * @return		RESET if UART is not busy, otherwise return SET.
 **********************************************************************/
FlagStatus UART_CheckBusy ()
{
	if (LPC_UART0->LSR & UART_LSR_TEMT)
		return RESET;
	else
		return SET;
}
